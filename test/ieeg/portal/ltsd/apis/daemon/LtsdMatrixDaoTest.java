package ieeg.portal.ltsd.apis.daemon;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import ieeg.portal.ltsd.apis.domain.Matrix2D;
import ieeg.portal.ltsd.apis.portal.PortalAccessObject;

import org.junit.Before;
import org.junit.Test;

public class LtsdMatrixDaoTest {

	private final String username = "watkinst";
	private final String password = "CIS573";	
	private final String snapName = "Study 017";
	PortalAccessObject portal;
	
	@Before
	public void setUp() throws Exception {
		portal = new PortalAccessObject(username, password);
	}

	@Test
	public void testTwoElementMatrix() {
		double[][] expectedArray = { {0.0, 0.0118743750251638}, {0.0118743750251638, 0.0}};
		int start = 4;
		int end = 6;
		
		LtsdMatrixDao dao = portal.getLtsdMatrixDao();
		Matrix2D actualMatrix2d = dao.getLtsdMatrixBySnapName(snapName, start, end);
		
		for (int i = 0; i<expectedArray.length; i++)
			assertArrayEquals(expectedArray[i], actualMatrix2d.data[i], 0.00001);		
	}
	
	@Test
	public void testNull() {
		int start = 0;
		int end = 0;
		
		LtsdMatrixDao dao = portal.getLtsdMatrixDao();
		Matrix2D actualMatrix2d = dao.getLtsdMatrixBySnapName(snapName, start, end);
		
		assertEquals(0, actualMatrix2d.data.length);
	}
	
	@Test
	public void testOneElementMatrix() {
		double[][] expectedArray = { {0.0} };
		int start = 4;
		int end = 5;
		
		LtsdMatrixDao dao = portal.getLtsdMatrixDao();
		Matrix2D actualMatrix2d = dao.getLtsdMatrixBySnapName(snapName, start, end);
		
		for (int i = 0; i<expectedArray.length; i++)
			assertArrayEquals(expectedArray[i], actualMatrix2d.data[i], 0.00001);	
	}

}
