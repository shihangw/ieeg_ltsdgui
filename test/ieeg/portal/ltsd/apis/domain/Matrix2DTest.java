package ieeg.portal.ltsd.apis.domain;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class Matrix2DTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testMatrix2D() {
		int nRows = 2;
		int nColumns = 5;
		Matrix2D matrix2d = new Matrix2D(nRows, nColumns);
		assertEquals(nRows, matrix2d.data.length);
		assertEquals(nColumns, matrix2d.data[0].length);		
	}

	@Test
	public void testMatrix2DnRow0() {
		int nRows = 0;
		int nColumns = 5;
		Matrix2D matrix2d = new Matrix2D(nRows, nColumns);
		assertEquals(nRows, matrix2d.data.length);	
	}
}
