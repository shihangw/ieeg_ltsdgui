/**
 * 
 */
package ieeg.portal.ltsd.apis.domain;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Shihang Wei
 *
 */
public class SeizureTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		int startIndex = 0;
		int endIndex = 5;
		String studyName = "foo";
		Seizure seizure = new Seizure(startIndex, endIndex, studyName);
		assertEquals(startIndex, seizure.startIndex);
		assertEquals(endIndex, seizure.endIndex);
		assertEquals(studyName, seizure.fileName);
	}

}
