package ieeg.portal.ltsd.apis.domain;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;


public class SimpleDataSetTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		int index = 0;
		String identifier = "testing";
		String filePath = "testing";		
		
		double[][] data = new double[16][1];
		double[] singleChan = {1, 2, 3, 4 };
		for (int i=0; i<16; i++) {
			data[i] = singleChan;
		}
		String studyName = "test";
		Seizure seizure = new Seizure(0, 0.0, 0.1, data, studyName);
		List<Seizure> seizures = new ArrayList<Seizure>();
		seizures.add(seizure);
		
		int windowSize = 1;
		int nOverlap = 0;
		
		SimpleDataSet simpleDataSet = new SimpleDataSet(index, identifier, filePath, seizures, windowSize, nOverlap);
		Matrix2D matrix2d = simpleDataSet.makeLTSDMatrix();
		matrix2d.printMatrix();
		
	}

}
