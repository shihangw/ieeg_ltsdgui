package ieeg.portal.ltsd.shared;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class LTSDAnalyzedSelectionDataTest {

	@Before
	public void setUp() throws Exception {
	}

	@SuppressWarnings("deprecation")
	@Test
	public void test() {
		boolean isTime = false;
		int startMark = 4;
		int endMark = 5;
		double[][] resultMatrix = new double[1][1];
		String image = "dasv";
		LTSDAnalyzedSelectionData analyzedSelectionData = new LTSDAnalyzedSelectionData(isTime, startMark, endMark, resultMatrix, image);
		
		assertEquals(isTime, analyzedSelectionData.getIsTime());
		assertEquals(startMark, analyzedSelectionData.getStartMark());
		assertEquals(endMark, analyzedSelectionData.getEndMark());
		assertEquals(resultMatrix, analyzedSelectionData.getResultMatrix());
		assertEquals(image, analyzedSelectionData.getImage());
		
	}

}
