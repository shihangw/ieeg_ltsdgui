package ieeg.portal.ltsd.shared;

import java.io.Serializable;

public class LTSDMetaData implements Serializable {

	// may need to generate a new value for this using Eclipse
	private static final long serialVersionUID = 7526472295622776147L;

	// Fields
	private int numSeizures;
	private int numAnnotations;
	private int numChannels;
	private String patientName;
	private String experimentName;
	private String date;
	private String lengthInHrs;
	
	
	public LTSDMetaData() {		
	}
	
	public LTSDMetaData(int numSeizures, int numChannels) {
		this.numSeizures = numSeizures;
		this.numChannels = numChannels;	
	}

	public int getNumSeizures() {
		return numSeizures;
	}

	public int getNumChannels() {
		return numChannels;
	}

}