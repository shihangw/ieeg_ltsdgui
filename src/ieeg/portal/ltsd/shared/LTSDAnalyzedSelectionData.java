package ieeg.portal.ltsd.shared;

import java.io.Serializable;

@SuppressWarnings("serial")
public class LTSDAnalyzedSelectionData implements Serializable {
	
	private boolean isTime;
	private int startMark;
	private int endMark;
	private double[][] resultMatrix;
	private String image;
	
	String snapId = "Study 017";

	public LTSDAnalyzedSelectionData() {
	}
	
	public LTSDAnalyzedSelectionData(boolean isTime, int startMark, int endMark, double[][] resultMatrix, String image){
		this.isTime = isTime;
		this.startMark = startMark;
		this.endMark = endMark;
		this.resultMatrix = resultMatrix;
		this.image = image;
		
	}
	
	public double[][] getResultMatrix() {
		return this.resultMatrix;
	}
	
	public String getSnapId() {
		return this.snapId;
	}
	
	public boolean getIsTime() {
		return this.isTime;
	}
	
	public boolean getIsIndex() {
		return !this.isTime;
	}
	
	public int getStartMark() {
		return this.startMark;
	}
	
	public int getEndMark() {
		return this.endMark;
	}
	
	public String getImage() {
		return this.image;
	}
	
}
