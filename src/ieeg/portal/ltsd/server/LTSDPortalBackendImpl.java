package ieeg.portal.ltsd.server;

import ieeg.portal.ltsd.apis.daemon.LtsdMatrixDao;
import ieeg.portal.ltsd.apis.portal.PortalAccessObject;
import ieeg.portal.ltsd.client.LTSDPortalBackend;
import ieeg.portal.ltsd.shared.LTSDAnalyzedSelectionData;
import ieeg.portal.ltsd.shared.LTSDMetaData;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.apporiented.algorithm.clustering.DendrogramImageGenerator;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import edu.upenn.cis.db.mefview.services.CountsByLayer;
import edu.upenn.cis.db.mefview.services.TimeSeries;
import edu.upenn.cis.db.mefview.services.TimeSeriesAnnotation;
import edu.upenn.cis.db.mefview.services.TimeSeriesInterface;


@SuppressWarnings("serial")
public class LTSDPortalBackendImpl extends RemoteServiceServlet implements
	LTSDPortalBackend {

	final String tsiPropertiesDir = "ieeg/portal/ltsd/shared";
	TimeSeriesInterface link;
	PortalAccessObject portal;
	private String nameOfStudy;
	
	@Override
	public LTSDMetaData getStudyFromLogin(String username, String password,
			String domainURL, String nameOfStudy)
			throws IllegalArgumentException {

		portal = new PortalAccessObject(username, password);
		portal.intializeTimeSeriesInterface(domainURL);
		
//		TimeSeriesInterface tsi = null;
//		try {
//			tsi = TimeSeriesInterface.newInstance(domainURL, username, password,
//					tsiPropertiesDir);
//		} catch (UnknownHostException e) {
//			if (tsi == null) {
//			e.printStackTrace();
//			throw new RuntimeException("Bad request: No TimeSeriesInterface "
//					+ "returned");  
//		}
//		else System.out.println("tsi is NOT null!");
//		}
//
//		if (tsi != null)
//			System.out.println("tsi is NOT null!");
//		link = tsi;
		return  makeLTSDMetaData(portal, nameOfStudy);
		//return "";
	}
	
	
	@Override
	public LTSDAnalyzedSelectionData getSelectionCriteria(boolean isTime, int from,
			int to)
			throws IllegalArgumentException {
		//TODO bounds check
		LtsdMatrixDao dao = portal.getLtsdMatrixDao();
		double[][] resultMatrix = dao.getLtsdMatrixBySnapName(this.nameOfStudy, from, to).data;
		//TODO 
		
		byte[] image = DendrogramImageGenerator.resultMatrixToByteArray(resultMatrix);
		
		
		return  makeLTSDSelectionData(isTime, from, to, resultMatrix, image);
		//return "";
	}
	
	public LTSDAnalyzedSelectionData makeLTSDSelectionData(boolean isTime, int from, int to, double[][] resultMatrix, byte[] image)  {
		return new LTSDAnalyzedSelectionData(isTime, from, to, resultMatrix, byteArrayToImage(image));
	}
	
	
	
	private LTSDMetaData makeLTSDMetaData(PortalAccessObject portal, String nameOfStudy) {
		TimeSeriesInterface tsi = portal.getTimeSeriesInterface();
		String snapshotID = tsi.getDataSnapshotIdByName(nameOfStudy);
		this.nameOfStudy = nameOfStudy;

		int numSeizures = getSzAnnotationList(tsi, snapshotID).size();
		int numChannels = tsi.getDataSnapshotTimeSeriesDetails(snapshotID).size();
		
		return new LTSDMetaData(numSeizures, numChannels);	
	}
	
	
	private List<TimeSeriesAnnotation> getSzAnnotationList (TimeSeriesInterface tsi, String snapID) {
		//Checks for multiple layers removed, see original long code
		TimeSeries[] timeSeriesArray = tsi.getDataSnapshotTimeSeries(snapID);
		String layer = getLayer(tsi, snapID);
		List<TimeSeriesAnnotation> annotationList = tsi.getTsAnnotations(snapID, 0, layer, timeSeriesArray, 0, 100);
		List<TimeSeriesAnnotation> element;
		int startIndex = 100;
		long startTime = 0;
		int elementSize = 1;
		while (elementSize > 0) {
			element = tsi.getTsAnnotations(snapID, startTime, layer, timeSeriesArray, startIndex, 100);
			elementSize = element.size();
			startIndex += 100;
			annotationList.addAll(element);
			startTime =  annotationList.get((annotationList.size() - 1)).getEndTimeUutc() + 1;
		}
		//Clean annotations
		Iterator<TimeSeriesAnnotation> annotationIterator = annotationList.iterator();
		TimeSeriesAnnotation annotation;
		while (annotationIterator.hasNext()) {
			annotation = annotationIterator.next();
			if ( ! annotation.getType().equals("Seizure")) {
				System.out.println("REMOVING NON-SEIZURE ANNOTATION " + annotation.getType());
				annotationIterator.remove();
			}
		}
		return annotationList;
	}

	private String getLayer (TimeSeriesInterface tsi, String snapID) {
		CountsByLayer layers = tsi.getCountsByLayer(snapID);
		Map<String, Long> annotationsMap = layers.getCountsByLayer(); 
		String layer = "default";
		for (String annotation: annotationsMap.keySet()) {
			System.out.println(annotation);
			//Fix this for multiple layers
			layer = annotation;
			if (annotation.equals("Seizure")) {
				//CHECK IF ANNOTATION is of type seizure
				//NB assume annotations present on all channels? Check this? 
				//check type property of annotation is that of Seizure, could be artifact and we don't want to include this
				//ideally have a seizure layer only but not enforced and not present
				System.out.println("Annotation is of type seizure?");
			}
		}
		return layer;		
	}
	
	private String byteArrayToImage( byte[] byteArr ) {
//		 SET CLASS VARIABLE TO IMAGE AFTER BEING CONVERTED
//		 FROM THE BYTE[].
		String base64Image = new String(org.apache.commons.codec.binary.Base64.encodeBase64(byteArr));
		
		int nPad = base64Image.length() - base64Image.indexOf('=', base64Image.length()-2);
		int remainder = base64Image.length()%4;
		System.out.println("nPad:"+nPad+";remainder:"+remainder);
		System.out.println("base64Image.length:"+base64Image.length());
		switch (remainder) {
		case 1:
			//chop the last '='
			base64Image = base64Image.substring(0, base64Image.length()-1);
			break;
		case 2:
			//chop last 2 '=' or pad two '='
			if (nPad==0)
				base64Image = base64Image+"==";
			else
				base64Image = base64Image.substring(0,base64Image.length()-2);
			break;
		case 3:
			//add one more '='
			base64Image = base64Image+"=";
			break;
		}
		//base64Image.length()%4
		return "data:image/png;base64," + base64Image;
		}
	
}


