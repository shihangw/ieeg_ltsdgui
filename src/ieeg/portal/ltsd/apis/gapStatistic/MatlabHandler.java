package ieeg.portal.ltsd.apis.gapStatistic;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JFileChooser;

import com.jmatio.io.MatFileReader;
import com.jmatio.types.MLArray;
import com.jmatio.types.MLDouble;
import com.jmatio.types.MLInt16;

/**
 * @author thomas watkins
 * Manages reading matrices from .mat files, can read a particular matrix or all of them ignoring other
 * file types.
 * Also offers ability to print out contents of.mat file using java.
 * Created using the JmatIO library credit to Wojciech Gradkowski
 */
public class MatlabHandler {
	//TODO error checking
	//What kinds of inputs can we get from matlab? how to handle different types
	//Matlab output function at some point?


	/**
	 * Prints the contents and types of a .mat file
	 * @throws IOException if file does not exist or is not of type .mat
	 */
	public static void printMatContents() throws IOException{
		//check extension is .mat
		File matFile = getJFileChooser();
		MatFileReader matFileReader = getMatFileReader(matFile);
		HashMap<String, MLArray> fileContents  = (HashMap<String, MLArray>) matFileReader.getContent();
		ArrayList<String> variableNames = new ArrayList<String>();
		variableNames.addAll(fileContents.keySet());
		int[] elementDimensions;
		String currentElement;
		String currentElType;
		for (int i = 0; i < variableNames.size(); i++) {
			currentElement = variableNames.get(i);
			currentElType = fileContents.get(currentElement).getClass().toString();
			System.out.println(currentElement + " is an instance of " + currentElType);
			if (currentElType.equals("class com.jmatio.types.MLDouble") || currentElType.equals("class com.jmatio.types.MLCell") || currentElType.equals("class com.jmatio.types.MLInt16")){
				elementDimensions = fileContents.get(currentElement).getDimensions();
				System.out.println("with dimensions of " + elementDimensions[0] + " " + elementDimensions[1]);
			}
			//TODO add specifics for dimensions of other types.
		}
	}

	public static int getNoColumnsKnownMatrix (String path, String variableName) throws IOException {
		//TODO check for wrong types
		File matFile = new File(path);
		MatFileReader matFileReader = new MatFileReader(matFile);
		int[] elementDimensions;
		HashMap<String, MLArray> fileContents  = (HashMap<String, MLArray>) matFileReader.getContent();
		elementDimensions = fileContents.get(variableName).getDimensions();
		if (elementDimensions == null) {
			throw new RuntimeException("No variable called " + variableName + " held in file: " + path);
		}
		return elementDimensions[1];	
	}

	/**
	 *Using a filechooser reads in all the matrices from a .mat file into a map which can then be accessed by the user. 
	 * @throws IOException 
	 * @return HashMap<String, double[][]> of all matrices contained in the original matlab file.
	 * @throws IOException if file does not exist or is not of type .mat
	 */
	public static HashMap<String, double[][]> getAllMatrices() throws IOException{
		//Assumes unique names
		//Check that can take MLArrays as well as MLDoubles
		File matFile = getJFileChooser();
		MatFileReader matFileReader = getMatFileReader(matFile);
		HashMap<String, MLArray> fileContents  = (HashMap<String, MLArray>) matFileReader.getContent();
		ArrayList<String> variableNames = new ArrayList<String>();
		variableNames.addAll(fileContents.keySet());
		HashMap<String, double[][]> convFileCont = new HashMap<String, double[][]>();
		String currentElement;
		String currentElType;
		for (int i = 0; i < variableNames.size(); i++) {
			currentElement = variableNames.get(i);
			currentElType = fileContents.get(currentElement).getClass().toString();
			if (currentElType.equals("class com.jmatio.types.MLDouble") || currentElType.equals("class com.jmatio.types.MLArray")){
				convFileCont.put(currentElement, getKnownMatMatrix(currentElement, matFile));
			}	
		}
		return convFileCont;
	}

	/**
	 * Using a filepath reads in all the matrices from a .mat file into a hashMap which can then be accessed by the user. 
	 * @throws IOException 
	 * @param path, String denoting path to a file 
	 * @return HashMap<String, double[][]> of all matrices contained in the original matlab file.
	 * @throws IOException if file does not exist or is not of type .mat
	 */
	public static HashMap<String, double[][]> getAllMatrices (String path) throws IOException{
		//Assumes unique names
		//Check that can take MLArrays as well as MLDoubles
		File matFile = new File(path);
		MatFileReader matFileReader = getMatFileReader(matFile);
		HashMap<String, MLArray> fileContents  = (HashMap<String, MLArray>) matFileReader.getContent();
		ArrayList<String> variableNames = new ArrayList<String>();
		variableNames.addAll(fileContents.keySet());
		HashMap<String, double[][]> convFileCont = new HashMap<String, double[][]>();
		String currentElement;
		String currentElType;
		for (int i = 0; i < variableNames.size(); i++) {
			currentElement = variableNames.get(i);
			currentElType = fileContents.get(currentElement).getClass().toString();
			if (currentElType.equals("class com.jmatio.types.MLDouble") || currentElType.equals("class com.jmatio.types.MLArray")) {
				convFileCont.put(currentElement, getKnownMatMatrix(currentElement, matFile));
			}	
		}
		return convFileCont;
	}

	public static double[][] getUnknownSingleMatrix (String path) throws IOException, RuntimeException {
		File matFile = new File(path);
		MatFileReader matFileReader = getMatFileReader(matFile);
		HashMap<String, MLArray> fileContents  = (HashMap<String, MLArray>) matFileReader.getContent();
		ArrayList<String> variableNames = new ArrayList<String>();
		variableNames.addAll(fileContents.keySet());

		if (variableNames.size() > 1) {
			throw new RuntimeException("mat file has multiple variables in it!");
		}

		String variableName = variableNames.get(0);
		String contentType = fileContents.get(variableName).getClass().toString();

		if (! contentType.equals("class com.jmatio.types.MLDouble") ||  ! contentType.equals("class com.jmatio.types.MLArray")) {
			throw new RuntimeException("mat file does not contain a matrix! rather: " + contentType);
		}

		double[][] data = ((MLDouble) matFileReader.getMLArray(variableName)).getArray();
		return data;
	}

	/**
	 * Starts a jfilechooser which allows user to select a .mat file containing the known (max 2D) matrix variable
	 * whose name is passed to the function as a parameter, it then returns this as a 2D Array of doubles.
	 * @param variableName string which is the name of the variable in MATLAB
	 * @return data double[][] which is the matrix.
	 * @throws IOException if file does not exist or is not of type .mat
	 */
	public static double[][] getKnownMatMatrix(String variableName) throws IOException{
		//TO DO what if variable not in file
		//TO DO what if variable not a matrix
		File matFile = getJFileChooser();
		MatFileReader matFileReader = getMatFileReader(matFile);
		double[][] data = ((MLDouble) matFileReader.getMLArray(variableName)).getArray();
		if(data == null){
			throw new IOException("No matrix of this name held in " + matFile.getName());
		}
		//System.out.println(data.length +" " + data[0].length + " " + data[0][0]);
		return data;
	}

	/**
	 * Overloaded method which accepts a File object representing a .mat file containing the known (max 2D) matrix variable
	 * whose name is passed to the function as a parameter, it then returns this as a 2D Array of doubles.
	 * @param variableName string which is the name of the variable in MATLAB
	 * @param matFile, accepts a File object to get the matrix from
	 * @return data double[][] which is the matrix.
	 * @throws IOException if file does not exist or is not of type .mat 
	 */
	public static double[][] getKnownMatMatrix(String variableName, File matFile) throws IOException{
		MatFileReader matFileReader = new MatFileReader(matFile);
		double[][] data = ((MLDouble) matFileReader.getMLArray(variableName)).getArray();
		if(data == null){
			throw new IOException("No matrix of this name held in " + matFile.getName());
		}
		//System.out.println(data.length +" " + data[0].length + " " + data[0][0]);
		return data;
	}

	/**
	 * Overloaded method which accepts a path to a file representing a .mat file containing the known (max 2D) matrix variable
	 * whose name is passed to the function as a parameter, it then returns this as a 2D Array of doubles.
	 * @param variableName string which is the name of the variable in MATLAB
	 * @param path, String from which it creates a File object to get the matrix from
	 * @return data double[][] which is the matrix.
	 * @throws IOException if file does not exist or is not of type .mat
	 */
	public static double[][] getKnownMatMatrix(String variableName, String path) throws IOException{
		File matFile = new File(path);
		MatFileReader matFileReader = new MatFileReader(matFile);
		double[][] data = ((MLDouble) matFileReader.getMLArray(variableName)).getArray();
		if(data == null){
			throw new IOException("No matrix of this name held in " + matFile.getName());
		}
		//System.out.println(data.length +" " + data[0].length + " " + data[0][0]);
		return data;
	}


	/**
	 * Overloaded method which accepts a path to a file representing a .mat file containing the known (max 2D) matrix variable
	 * whose name is passed to the function as a parameter, it then returns this as a 2D Array of doubles.
	 * @param variableName string which is the name of the variable in MATLAB
	 * @param path, String from which it creates a File object to get the matrix from
	 * @return data double[][] which is the matrix.
	 * @throws IOException if file does not exist or is not of type .mat
	 */
	public static double[][] getKnownMatInt16Matrix(String variableName, String path, boolean flip) throws IOException{
		File matFile = new File(path);
		MatFileReader matFileReader = new MatFileReader(matFile);

		short[][] rawData = ((MLInt16) matFileReader.getMLArray(variableName)).getArray();

		if(rawData == null){
			throw new IOException("No matrix of this name held in " + matFile.getName());
		}
		//Convert shorts to double
		//assume all same length
		System.out.println("Converting MLInt16 array " + path + " to java double array");

		double[][] data;
		if (flip){
			data = switchRowColumn(rawData);
		} else {
			data = new double[rawData.length][rawData[0].length] ;
			for (int i = 0; i < rawData.length; i++) {
				for (int j = 0; j < rawData[0].length; j++) {
					data[i][j] =  (double) rawData[i][j];
				}
			}
		}

		System.out.println(data.length +" " + data[0].length + " " + data[0][0]);
		return data;
	}




	/**
	 * Helper function, if matlab data saved so matrix needs to be flipped and cast to double.
	 * @param originalArray
	 * @return
	 */
	public static double[][] switchRowColumn (short[][] originalArray) {
		int matRows = originalArray.length;
		int matColumns = originalArray[0].length;
		//create an array to switch them
		double[][] flippedArray = new double[matColumns][matRows];
		for (int i = 0; i < matColumns; i++) {//Going to 104
			for (int j = 0; j < matRows; j++) { //Going to 132000
				flippedArray[i][j] = (double) originalArray[j][i];
			}	
		}
		return flippedArray;

	}



	/**
	 * Helper eliminating boiler plate to get a file from a JFileChooser. Does not check filetype.
	 * @return File, file opened by JfileChooser
	 */
	static File getJFileChooser(){
		/*JFileChooser chooser =  new JFileChooser();
		int chooseReturnVal = chooser.showOpenDialog(null);
		File matFile = chooser.getSelectedFile();
		if (chooseReturnVal == JFileChooser.APPROVE_OPTION) {
			System.out.println("Opening: " + chooser.getSelectedFile().getName());
		}*/
		return null;//matFile;
	}

	/**
	 * Creates a MatFileReader for a file passed to the function if it's extension is .mat.
	 * @param file, file to create MatFileReader for.
	 * @return MatFileReader for that particular file
	 * @throws IOException
	 */
	private static MatFileReader getMatFileReader(File file) throws IOException{
		if(!checkExtMat(file)){
			throw new IOException("File's extension is not .mat");
		}
		MatFileReader matFileReader = new MatFileReader(file);
		return matFileReader;
	}

	/**
	 * Checks whether a file ends in .mat.
	 * @param file
	 * @return boolean, true if ext is .mat false otherwise
	 */
	public static boolean checkExtMat(File file){
		//Needs further testing
		String fileName = file.getName();
		String extension = "";
		int i = fileName.lastIndexOf('.');
		if (i > 0) {
			extension = fileName.substring(i + 1);
			if (extension.equals("mat")) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}	
	}

	/**
	 * Checks whether a file ends in .mat.
	 * @param file
	 * @return boolean, true if ext is .mat false otherwise
	 */
	public static boolean checkExtMat(String file){
		//Needs further testing
		String extension = "";
		int i = file.lastIndexOf('.');
		if (i > 0) {
			extension = file.substring(i + 1);
			if (extension.equals("mat")) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}	
	}

}
