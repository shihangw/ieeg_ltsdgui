package ieeg.portal.ltsd.apis.gapStatistic;

import java.io.IOException;
import java.util.Arrays;

import marf.math.MathException;


/**
 * @author thomaswatkins
 *
 *Credit to Mark Lippman
 */
public class LTSDCalc {

	int windowSize;
	int nOverlap;
	
	

	public static void main(String[] args) throws IOException {
		//Thoughts:
//		//Make all of this a method in the dataSet class
//		int seizureOnsetSampleIndex = 5 * 60 * 200; //known format, 5 mins * 60 seconds * 200 samples per second
//		int ictalStartIndex = seizureOnsetSampleIndex - 30 * 200; //go back 30 seconds in sample
//		int ictalEndIndex = seizureOnsetSampleIndex + 90 * 200; //go forward 90 seconds in sample
//		DataSetCollection dataSetCollection = new DataSetCollection();
//		dataSetCollection.initializeDataSets();
//		System.out.println("Got here");
//	
//		for (DataSet dataSet: dataSetCollection.allDataSets) {
//
//			//get the minimum no of channels so can get a selection of the data.
//			dataSet.getMinNumChannels();
//
//			for (Seizure seizure : dataSet.seizures) {
//
//				dataSet.getSingleSeizureRawData(seizure.fileName);
//			
//				//Here we have the issue that we may want the start and end indices to vary for each
//				//seizure file. For now assume they will be the same and worked out as above
//				dataSet.cleanSingleSeizureRawData(ictalStartIndex, ictalEndIndex);
//
//				dataSet.addToSzVectorCache(dataSet.currentSeizure);
//
//			}
//			dataSet.makeLTSDMatrix();
//			dataSet.printDistanceMatrix();
//		}

	}

	

		
	
	
	public LTSDCalc(int windowSize, int nOverlap){
		//TODO check that overlap not greater than windowSize
		this.windowSize = windowSize;
		this.nOverlap = nOverlap;	
	}
	



	public double[] getAllChannelsRawSzVector (double[][] allChanRawSz) {
		//all seizure vectors should be same length, can actually work this out up front when we find the mi
		System.out.println("Called: getAllChannelsRawSzVector");
		double[] singleChannelSzVector = getSingleChannelRawSzVector(allChanRawSz[0]);
		int singleVectorLen = singleChannelSzVector.length;
		double[] allChannelsSzVector = new double[singleVectorLen * allChanRawSz.length];
		int destPos = 0;
		System.arraycopy(singleChannelSzVector, 0, allChannelsSzVector, destPos, singleVectorLen);
		destPos += singleVectorLen;
		for (int i = 1; i < allChanRawSz.length; i++) {
			singleChannelSzVector = getSingleChannelRawSzVector(allChanRawSz[i]);
			System.arraycopy(singleChannelSzVector, 0, allChannelsSzVector, destPos, singleVectorLen);
			destPos += singleVectorLen;
		}
		return allChannelsSzVector;
	}
	
	/**
	 * Takes a vector representing a single channel recording of a seizure and analyzes it by performing a hamming window 
	 * @param rawSzDoubles
	 * @return
	 */
	public double[] getSingleChannelRawSzVector (double [] rawSzDoubles) {
		System.out.println("Called: getSingleChannelRawSzVector");
		//holder for raw results
		double[][] pSDensityMatrix;
		//subtract the median from each value
		addOverArray(rawSzDoubles, - getMedian(rawSzDoubles));
		//work out robustStd using median absolute deviation
		double robustSTD = 1.4826 * getMedAbsDev(rawSzDoubles);
		double threshold = 3 * robustSTD;
		for (int l = 0; l < rawSzDoubles.length; l++) {
			if (rawSzDoubles[l] > threshold || rawSzDoubles[l] < -threshold) {
				rawSzDoubles[l] = 0;
			}
		}
		pSDensityMatrix = sTFT(rawSzDoubles, (10*200), 1000);
		double[] rawSzVector = twoDToOneD(pSDensityMatrix);
		return rawSzVector;
	}


	/**
	 * Performs the log-time-spectral-distance on the raw seizure vectors of two separate seizures to compute one
	 * element of the final matrix.
	 * @param seizureX, double array from rawSeizure vector function, stft?
	 * @param seizureY, double array from rawSeizure vector function, stft?
	 * @return tempd, double result to be put into the ltsd matrix.
	 */
	public double calculateLTSDMatrixVal (double[] seizureX, double[] seizureY) {
		System.out.println("Called: calculateLTSDMatrixVal");
		//check that arrays are same size
		if (seizureX.length != seizureY.length) {
			throw new RuntimeException("X and Y seizure vectors aren't of equal size!");
		} else if (seizureX.length == 0 || seizureY.length == 0) {
			throw new RuntimeException("One of the seizure vectors has no values!");
		}
		int normalizer = 0;
		double tempSum = 0;//Is being 0 correct?
		double tempd;
		for (int i = 0; i < seizureX.length; i++) {
			if (seizureX[i] != 0 && seizureY[i] != 0) {
				tempSum +=  Math.pow( (Math.log10( seizureX[i] / seizureY[i] ) * 10 ) , 2 ) ;
				normalizer += 1;
			}
		}
		//originally a double sum? in case it was a matrix but both should be vectors\
		//check double summing whether it's necessary
		tempd = Math.sqrt(tempSum) / normalizer;
		return tempd;
	}

	/**
	 * Helper for sTFT, calculates number of windows required of correct size needed to perform the short time fourier transform.
	 * @param numVals, length of the array of doubles passed to 
	 * @param windowSize
	 * @param nOverlap
	 * @return
	 */
	private int getNumWindowsRequired (int numVals, int windowSize, int nOverlap) {
		int valid = numVals - windowSize;
		int stepSize = windowSize - nOverlap;
		int numWindows = numVals / stepSize;
		if (valid % stepSize != 0) {
			numWindows += 1;
		}
		return numWindows;	
	}

	/**
	 * Performs short time fourier transform on the data passed to it and returns a power spectral density matrix.
	 * Note if window size passed to the matrix is not a power of two it will be converted.
	 * This function is an attempt to recreate some of the functionality of the MATLAB spectrogram method but doesn't
	 * take the parameters of hertz or samplingfrequency as they are not used.
	 * Original header:
	 * (double[] values, int windowSize, int nOverlap, int hertz, int samplingFreq) 
	 * 
	 * @param values array of double values representing signal to be analyzed.
	 * @param windowSize size in to which the signal be broken down and analyzed.
	 * @param nOverlap, number of elements by which the windows of analysis on the signal will overlap.
	 * @return double[][], power spectral densoty matrix
	 */
	public double[][] sTFT (double[] values, int windowSize, int nOverlap) {	
		int adjWindowSize = getClosestTwoPower(windowSize);
		int numWindows = getNumWindowsRequired(values.length, adjWindowSize, nOverlap);
		int stepSize = adjWindowSize - nOverlap;
		double [] windowWeights = getHammingWindow(adjWindowSize);
		double [][] allWindows = new double[numWindows][adjWindowSize];
		double [][] allWindowsResults = new double[numWindows][adjWindowSize];
		int startInd = 0;
		int endInd = adjWindowSize;
		int windowIndex = 0;
		while (startInd < values.length) {
			allWindows[windowIndex] = Arrays.copyOfRange(values, startInd, endInd);
			windowIndex += 1;
			startInd += stepSize;
			endInd = startInd + adjWindowSize;		
		}
		// scale each element of each window by relevant weights
		for (int i = 0; i < numWindows; i++) {
			for (int j = 0; j < windowSize; j ++) {
				allWindows[i][j] *= windowWeights[j];
			}
		} 
		//now ffts....
		for (int i = 0; i < numWindows; i++) {
			try {
				//Find fft that doesn't need two take in two arrays?
				marf.math.Algorithms.FFT.normalFFT(allWindows[i], allWindowsResults[i]);
//				System.out.println(Arrays.toString(allWindows[i]));
//				System.out.println(Arrays.toString(allWindowsResults[i]));
			} catch (MathException e) {
				e.printStackTrace();
			}
		}
		//Power spectral density, square each result. Note scaling detailed in the Matlab code for spectrogram is not performed here.
		for (int i = 0; i < numWindows; i++) {
			for (int j = 0; j < windowSize; j ++) {
				allWindowsResults[i][j] = allWindowsResults[i][j] * allWindowsResults[i][j];
			}
		}
		return allWindowsResults;	
	}

	/**
	 * Calculates Hamming window weights and normalizes them so total adds up to 1.
	 * @param length of window you want
	 * @return double[] of weights corresponding to the array you wish to apply it to.
	 */
	public double[] getHammingWindow (int length) {
		double a = 0.54;
		double b = 0.46;
		double[] windowWeights = new double[length];
		double sum = 0;
		for (int i = 0; i < length; i++) {
			windowWeights[i] = a - b * Math.cos( ( 2 * Math.PI * i ) / ( length - 1 ) );
			sum += windowWeights[i];
		}
		for (int i = 0; i < length; i++) {
			windowWeights[i] =  windowWeights[i] / sum;
		}
		return windowWeights;	
	}


	/**
	 * Gives the first power of two whose value is greater than that of the paramter passed to it.
	 * @param val, a positive number
	 * @return the first power of two greater than the parameter.
	 * @exception RuntimeException if val is negative.
	 */
	public int getClosestTwoPower (int val) {
		if (val < 0) {
			throw new RuntimeException("Fuction does not accept negative values");
		}
		int answer = 2;
		do {
			answer = answer * 2;
		} while (answer < val);
		return answer;	
	}

	
	
	
	/**
	 * Converts a two dimensional array of doubles to a one dimensional array primitive doubles.
	 * @param matrix, 2 dimensional array of doubles of size x * y.
	 * @return oneD, 1 dimensional array array of doubles of size xy.
	 */
	public static double[] twoDToOneD (double[][] matrix) {

		int sizeOfVector = matrix.length * matrix[0].length;
		int k = 0;
		double[] oneD = new double[sizeOfVector];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				oneD[k] = matrix[i][j];
				k++;
			}
		}
		return oneD;
	}

	/**
	 * Converts a two dimensional array of doubles to a one dimensional array of the java wrapper class Double containing all the same values.
	 * @param matrix, 2 dimensional array of doubles of size x * y.
	 * @return oneD, 1 dimensional array array of Doubles of size xy.
	 */
	public static Double[] twoDToOneDwCastDoub (double[][] matrix) {
		int sizeOfVector = matrix.length * matrix[0].length;
		int k = 0;
		Double[] oneD = new Double[sizeOfVector];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				oneD[k] = (Double) matrix[i][j];
				k++;
			}
		}
		return oneD;
	}

	/**
	 * Converts a two dimensional array of doubles to a one dimensional array of ints containing all the same values.
	 * @param matrix, 2 dimensional array of doubles of size x * y.
	 * @return oneD, 1 dimensional array array of integers of size xy.
	 */
	public static int[] twoDToOneDwCastInt (double[][] matrix) {
		int sizeOfVector = matrix.length * matrix[0].length;
		int k = 0;
		int[] oneD = new int[sizeOfVector];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				oneD[k] = (int) matrix[i][j];
				k++;
			}
		}
		return oneD;
	}


	/**
	 * Modifies array passed to it by adding the specified value to every element in it. TODO replace with matrix mutiplication?
	 * @param array, to be modified
	 * @param value, mayb positive or negative
	 */
	public static void addOverArray (double[] array, double value) {
		for (int i = 0; i < array.length; i++) {
			array[i] += value;
		}
	}


	/**
	 * Deep copies the original array so as not to modify it and then calculates median absolute deviation following this formula: MAD = Median ( | Xi - Median(Xi) | )
	 * @param array, to be copied and used to find
	 * @return
	 */
	public static double getMedAbsDev (double[] array) {
		double[] arrayCopy = Arrays.copyOf(array, array.length);
		Arrays.sort(arrayCopy);
		double firstMedian;
		double absMedianDev;
		if (arrayCopy.length % 2 == 1) {
			firstMedian = arrayCopy[arrayCopy.length / 2];	
		} else {
			firstMedian = (arrayCopy[arrayCopy.length / 2] + arrayCopy[(arrayCopy.length / 2) +  1]) / 2;
		}
		for (int i = 0; i < array.length; i++) {
			arrayCopy[i] -= firstMedian;
			arrayCopy[i] = Math.abs(arrayCopy[i]);
		}
		Arrays.sort(arrayCopy);
		if (arrayCopy.length % 2 == 1) {
			absMedianDev = arrayCopy[arrayCopy.length / 2];	
		} else {
			absMedianDev = (arrayCopy[arrayCopy.length / 2 - 1] + arrayCopy[(arrayCopy.length / 2)]) / 2;
		}
		return absMedianDev;	
	}

	/**
	 * Returns median of an array without altering it.
	 * @param array
	 * @return median value double
	 */
	public static double getMedian (double[] array) {
		//COPY array then sort
		double[] arrayCopy = Arrays.copyOf(array, array.length);
		Arrays.sort(arrayCopy);
		double median;
		if (arrayCopy.length % 2 == 1) {
			median = arrayCopy[arrayCopy.length / 2];	
		} else {
			median = (arrayCopy[arrayCopy.length / 2 - 1] + arrayCopy[(arrayCopy.length / 2)]) / 2;
		}
		return median;
	}

	
	/**
	 * ONLY HERE FOR TESTING
	 * Performs fft on its first input and fills the second array with the result.
	 * @param input
	 * @param output
	 */
	public static void doFFT (double[] input, double[] output) {
		try {
			marf.math.Algorithms.FFT.normalFFT(input, output);
		} catch (MathException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
