package ieeg.portal.ltsd.apis.gapStatistic;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/*
 * @author thomaswatkins
 *
 */
public class FileIO {

	
	public static double[][] getSingleRawSeizureData (String fullFilePath, boolean needsToBeTransposed) throws IOException {
		//TODO create a version which doesn't need to know the variable name
		String variableName = "extractedSeizure";
		return (MatlabHandler.getKnownMatInt16Matrix(variableName, fullFilePath, needsToBeTransposed));
	}
	
	/**
	 * @param fullFilePath
	 * @return
	 * @throws IOException
	 */
	public static int getNumberOfChannelsFromSzFile (String fullFilePath) throws IOException{
		//TODO create a version which doesn't need to know the variable name
		//number of channels is the second dimension of the mat matrix representing data
		String variableName = "extractedSeizure";
		return MatlabHandler.getNoColumnsKnownMatrix(fullFilePath, variableName);
	}
	
	
	/**
	 * @return
	 */
	public static ArrayList<String> getDataSetIdentifiers () {
		return getMarksPatientsIdentifiers();
	}
	
	//Note can define which patients to work from here
	/**
	 * Gets the identifiers held in the CHOP_10pts_physicianClusterings.mat file. To decrease the number of patients being analyzed modify here.
	 * @return
	 */
	public static ArrayList<String> getMarksPatientsIdentifiers (){
		double[][] pts = null;
		try {
			pts = MatlabHandler.getKnownMatMatrix("pts","/Users/thomaswatkins/Documents/fourier/testData/test1/CHOP_10pts_physicianClusterings.mat");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(Arrays.deepToString(pts));
		int[] patientNumbers = LTSDCalc.twoDToOneDwCastInt(pts);
		ArrayList<String> patients = new ArrayList<String>();
		String patientString;
		for (int i = 0; i < patientNumbers.length; i++) {

			patientString = String.valueOf(patientNumbers[i]);
			if (patientString.length() == 1) {
				patientString = "0" + patientString;
			}
			patients.add(patientString);
		}
		for (int i = 0; i < patients.size(); i++) {
			System.out.println(patients.get(i));
		}
		//OVERRIDE AND JUST GET PATIENT TWO
		ArrayList<String> example = new ArrayList<String>();
		example.add(patients.get(1));
		//return patients;
		return example;
	}
	
	
	/**
	 * @param directory
	 * @param identifier
	 * @return
	 */
	public static List<String> getFileListFromIdentifier (File directory, String identifier) {
		String[] allDataSetFiles = directory.list();
		System.out.println(Arrays.deepToString(allDataSetFiles));
		ArrayList<String> DataSetFiles = new ArrayList<String>();
		String beginning;
		for (int i = 0; i < allDataSetFiles.length; i++) {
			if (MatlabHandler.checkExtMat(allDataSetFiles[i])) {
				beginning = allDataSetFiles[i].substring(0,2);
				int match = identifier.indexOf(beginning);
				if (match != -1) {
					System.out.println("Match: " + match);
					DataSetFiles.add((allDataSetFiles[i]));
				}
			}	
		}
		return DataSetFiles;
	}
	
	

	

}
