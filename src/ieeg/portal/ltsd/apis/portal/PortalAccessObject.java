package ieeg.portal.ltsd.apis.portal;

import ieeg.portal.ltsd.apis.daemon.LtsdMatrixDao;
import ieeg.portal.ltsd.apis.domain.Seizure;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import edu.upenn.cis.db.mefview.services.CountsByLayer;
import edu.upenn.cis.db.mefview.services.TimeSeries;
import edu.upenn.cis.db.mefview.services.TimeSeriesAnnotation;
import edu.upenn.cis.db.mefview.services.TimeSeriesDetails;
import edu.upenn.cis.db.mefview.services.TimeSeriesIdAndDCheck;
import edu.upenn.cis.db.mefview.services.TimeSeriesInterface;
import edu.upenn.cis.db.mefview.services.UnscaledTimeSeriesSegment;

public class PortalAccessObject {
	
	private final static String domainUrl = "https://view-qa.elasticbeanstalk.com/services";
	private final String tsiPropertiesDir = "ieeg/portal/ltsd/shared";
	
	private String userName;
	
	private String password;
	private LtsdMatrixDao ltsdMatrixDao;
	
	private TimeSeriesInterface tsi = null;
	
	public PortalAccessObject(String userName, String password) {
		this.userName = userName;
		this.password = password;
		intializeTimeSeriesInterface(domainUrl);
	}
	
	public LtsdMatrixDao getLtsdMatrixDao() {
		if (ltsdMatrixDao == null)
			synchronized (this) {
				if (ltsdMatrixDao == null)
					ltsdMatrixDao = new LtsdMatrixDao(this);				
			}
		return ltsdMatrixDao;
	}
	
	public TimeSeriesInterface getTimeSeriesInterface() {
		return tsi;
	}
	
	/**
	 * Portal integration
	 * Makes a call to the static factory method to get a new TimeSeriesInterface object. 
	 * At the moment contains a hard coded reference to the tsiProperties directory.
	 * This may need to be updated. Also what to do when
	 * @param url, web URL of server
	 * @param userName
	 * @param password
	 * @throws Runtime Exception if the request was not valid for any reason
	 * @return TimeSeriesInterface object if a valid request
	 */
	public TimeSeriesInterface intializeTimeSeriesInterface (String url) {
		//TODO implement a check that factor method worked, or do away with this?
		try {
			tsi = TimeSeriesInterface.newInstance(url, userName, password, tsiPropertiesDir);
		} catch (UnknownHostException e) {
			if (tsi == null) {
				e.printStackTrace();
				throw new RuntimeException("Bad request, no TimeSeriesInterface Returned");	
			}	
		}		
		return tsi;
	}
	
	/**
	 * 
	 *PORTAL INTEGRATION
	 * Gets all the EEG data for all channels from the portal for the notations with indices between the values
	 * of the parameters "from" and "to" and creates a Seizure object for each notation.
	 *TODO add checking for whether annotations are of seizure type or not.
	 *TODO add a limit on how many annotations can be given at any one time. Be wary of the data limit from portal.
	 * @param snapName, name of the study on the portal from which the annotations
	 * originate and the data will come from.
	 * @param from, start taking data from annotation at this index.
	 * @param to
	 * @throws Runtime Exception if from and to are illegal, ie -ve indices or there aren't any notations
	 * at these indices.  
	 * @return List of seizure objects which can then be used in the gap statistic calculation.
	 */
	public List<Seizure> getSzInIndexRange (String snapName, int from, int to) {
		//TODO error checking
		TimeSeriesInterface tsi = intializeTimeSeriesInterface(domainUrl);
		String snapID = tsi.getDataSnapshotIdByName(snapName);
		List<TimeSeriesIdAndDCheck> channelRevIDs = getChannelRevIDs(tsi, snapID);
		List<TimeSeriesAnnotation> szAnnotations = getSzAnnotationList(tsi, snapID);
		int numSz = szAnnotations.size();

		if (from < 0 || to < 0) throw new RuntimeException("Illegal index, can't be less than 0");
		if (to < from) throw new RuntimeException("to is less than from, illegal indexing");
		if (to > numSz) throw new RuntimeException("to is greater than number of seizures, index out of bounds");

		List<Seizure> szList = new ArrayList<Seizure>();
		double startTime, endTime;
		TimeSeriesAnnotation current;
		double[][] currentData;
		for (int i = from; i < to; i++) {
			current = szAnnotations.get(i);
			startTime = szAnnotations.get(i).getStartTimeUutc().doubleValue();
			endTime = szAnnotations.get(i).getEndTimeUutc().doubleValue();
			currentData = getRawDataSingleSz (tsi, snapName, channelRevIDs, current);
			szList.add(new Seizure(i, startTime, endTime, currentData, snapName));
		}
		return szList;
	}
	
	
	/**
	 * Helper method for finding number of annotations held in a study. At the moment assumes that all
	 * annotations are seizures.
	 *TODO add checking to make sure some annotations are seizures
	 * @param studyName, name of study on portal
	 * @return number of annotations in this timeseries annotation
	 */
	public int getSnapshotSzNum (String studyName) {
		TimeSeriesInterface tsi = intializeTimeSeriesInterface(domainUrl);
		String snapID = tsi.getDataSnapshotIdByName(studyName);
		System.out.println("snapID " + snapID);
		List<TimeSeriesAnnotation> szAnnotations = getSzAnnotationList(tsi, snapID);
		return szAnnotations.size();
	}

	
	/**
	 * portal
	 * Helper method to get TimeSeriesIdAndDCheck objects for each channel that is part of seizure.
	 * @param tsi
	 * @param snapID
	 * @return
	 */
	List<TimeSeriesIdAndDCheck> getChannelRevIDs (TimeSeriesInterface tsi, String snapID) {
		List<TimeSeriesDetails> channelList = tsi.getDataSnapshotTimeSeriesDetails(snapID);
		List<TimeSeriesIdAndDCheck> channelNames =  new ArrayList<TimeSeriesIdAndDCheck>();
		TimeSeriesIdAndDCheck curRevIDchID;
		for (TimeSeriesDetails tsd : channelList) {
			curRevIDchID = new TimeSeriesIdAndDCheck(tsd.getRevId(), tsd.getDataCheck());
			channelNames.add(curRevIDchID);
		}
		return channelNames;
	}
	

	
	List<TimeSeriesAnnotation> getSzAnnotationList (TimeSeriesInterface tsi, String snapID) {
		//Checks for multiple layers removed, see original long code
		TimeSeries[] timeSeriesArray = tsi.getDataSnapshotTimeSeries(snapID);
		String layer = getLayer(tsi, snapID);
		List<TimeSeriesAnnotation> annotationList = tsi.getTsAnnotations(snapID, 0, layer, timeSeriesArray, 0, 100);
		List<TimeSeriesAnnotation> element;
		int startIndex = 100;
		long startTime = 0;
		int elementSize = 1;
		while (elementSize > 0) {
			element = tsi.getTsAnnotations(snapID, startTime, layer, timeSeriesArray, startIndex, 100);
			elementSize = element.size();
			startIndex += 100;
			annotationList.addAll(element);
			startTime =  annotationList.get((annotationList.size() - 1)).getEndTimeUutc() + 1;
		}
		//Clean annotations
		Iterator<TimeSeriesAnnotation> annotationIterator = annotationList.iterator();
		TimeSeriesAnnotation annotation;
		while (annotationIterator.hasNext()) {
			annotation = annotationIterator.next();
			if ( ! annotation.getType().equals("Seizure")) {
				System.out.println("REMOVING NON-SEIZURE ANNOTATION " + annotation.getType());
				annotationIterator.remove();
			}
		}
		return annotationList;
	}
	


	/**
	 * Portal integration
	 * TODO, check the amount of time being analyzed/
	 * @param tsi
	 * @param snapName
	 * @param channelRevIDs
	 * @param annotation
	 * @return
	 */
	double[][] getRawDataSingleSz (TimeSeriesInterface tsi, String snapName, List<TimeSeriesIdAndDCheck> channelRevIDs, TimeSeriesAnnotation annotation) {
		String snapID = tsi.getDataSnapshotIdByName(snapName);
		//calculate start
		//Make modifications here, try 30 seconds before and 90 seconds afterwards
		//TODO error checking
		//old code:
//		Long startTime = annotation.getStartTimeUutc();
//		Long endTime = annotation.getEndTimeUutc();
//		Long duration = endTime - startTime;
//		double start =  startTime.doubleValue();
//		double length = duration.doubleValue();
		//New code:
		double start = annotation.getStartTimeUutc() - 8.1191103E7;
		double length = 2 * 8.1191103E7;
		System.out.println("length " + length);

		UnscaledTimeSeriesSegment[][] rawSeizureValues = tsi.getUnscaledTimeSeriesSetRaw(snapID, channelRevIDs, start, length , 1);
		double scalingFactor;
		int[] singleChanRawSzValues;
		int numChan = rawSeizureValues.length;
		System.out.println(numChan);
		int seizureLength = rawSeizureValues[0][0].getSeries().length;
		System.out.println(seizureLength);
		for (int i = 0; i < numChan; i++) {
			System.out.println(rawSeizureValues[i][0].getSeries().length);
		}
		UnscaledTimeSeriesSegment ts = rawSeizureValues[0][0];
		int[] szData = ts.getSeries();
		System.out.println("Series length " + szData.length);
		double[][] scaledSzValues = new double[numChan][seizureLength];
		UnscaledTimeSeriesSegment current;
		for (int i = 0; i < numChan; i++) {
			current = rawSeizureValues[i][0];
			scalingFactor = current.getScale();
			singleChanRawSzValues = current.getSeries();
			for (int j = 0; j < seizureLength; j++) {
				scaledSzValues[i][j] =  ((double) singleChanRawSzValues[j]) * scalingFactor;
			}	
		}
		return scaledSzValues;
	}
	
	/**
	 * portal integration
	 * The layer is required to to request the annotations from the portal. This method is called in get annotations. If we can make it shorter
	 * make it part of getAnnotations?
	 * @param tsi
	 * @param snapID
	 * @return
	 */
	String getLayer (TimeSeriesInterface tsi, String snapID) {
		CountsByLayer layers = tsi.getCountsByLayer(snapID);
		Map<String, Long> annotationsMap = layers.getCountsByLayer(); 
		String layer = "default";
		for (String annotation: annotationsMap.keySet()) {
			System.out.println(annotation);
			//Fix this for multiple layers
			layer = annotation;
			if (annotation.equals("Seizure")) {
				//CHECK IF ANNOTATION is of type seizure
				//NB assume annotations present on all channels? Check this? 
				//check type property of annotation is that of Seizure, could be artifact and we don't want to include this
				//ideally have a seizure layer only but not enforced and not present
				System.out.println("Annotation is of type seizure?");
			}
		}
		return layer;		
	}

}
