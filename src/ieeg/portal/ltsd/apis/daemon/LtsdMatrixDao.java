package ieeg.portal.ltsd.apis.daemon;

import ieeg.portal.ltsd.apis.domain.DataSet;
import ieeg.portal.ltsd.apis.domain.Matrix2D;
import ieeg.portal.ltsd.apis.domain.Seizure;
import ieeg.portal.ltsd.apis.domain.SimpleDataSet;
import ieeg.portal.ltsd.apis.portal.PortalAccessObject;
import ieeg.portal.ltsd.shared.LTSDAnalyzedSelectionData;

import java.util.List;

/**
 * @author Shihang Wei
 *
 */
public class LtsdMatrixDao{
	
	public static boolean isDaoReady = false;
	public static LtsdMatrixDao ltsdMatrixDao = null;

    PortalAccessObject portalEntryPoint;
    DataSet dataSet;
    
    public LtsdMatrixDao(PortalAccessObject portalEntryPoint) {
    	this.portalEntryPoint = portalEntryPoint;
    }
    
    /**
     * @param snapId
     * @param from
     * @param to
     * @return LTSD matrix of the dataset consisting of seizures from the study identified with snapId, and with indices falls in the range of [from,to)
     */
    public Matrix2D getLtsdMatrixBySnapName(String snapId, int from, int to) {
		List<Seizure> szList = portalEntryPoint.getSzInIndexRange(snapId, from, to);

		dataSet = new SimpleDataSet(0, "Study 005 subset", "Study 005", szList, 2000, 1000);		
		dataSet.makeLTSDMatrix();		
		return dataSet.getLTSDMatrix();
    }
    
    /**
     * @param selection
     * @return LTSD matrix of the dataset consisting of seizures satisfying the condtions specified by selection 
     */
    public Matrix2D getLtsdMatrixByProfile(LTSDAnalyzedSelectionData selection) {
    	
    	return getLtsdMatrixBySnapName(selection.getSnapId(), selection.getStartMark(),selection.getEndMark());
    }

}
