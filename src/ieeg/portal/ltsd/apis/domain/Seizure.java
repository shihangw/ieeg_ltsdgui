package ieeg.portal.ltsd.apis.domain;

/**
 * @author thomaswatkins
 *
 */
public class Seizure {
	//TODO update when know structure of it from portal.
	int startIndex;
	int endIndex;
	int index;
	double startTime;
	double endTime;
	String fileName;
	double[][] data;

	
	
	/**
	 * @param startIndex
	 * @param endIndex
	 * @param fileName
	 */
	public Seizure (int index, double startTime, double endTime, double[][] data, String studyName) {
		this.index =  index;
		this.startTime = startTime;
		this.endTime = endTime;
		this.fileName = studyName;
		this.data = data;
	}
	

	
	/**
	 * @param startIndex
	 * @param endIndex
	 * @param fileName
	 */
	public Seizure (int startIndex, int endIndex,  String studyName) {
		this.startIndex =  startIndex;
		this.endIndex = endIndex;
		this.fileName = studyName;
	}

}
