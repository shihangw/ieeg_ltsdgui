package ieeg.portal.ltsd.apis.domain;

import java.util.List;

/**
 * A DataSet is a group of annotations (Seizures)
 * @author Shihang Wei
 *
 */
public interface DataSet {
	
	/**
	 * @return Name of this DataSet
	 */
	public String getIdentifier();
	
	/** 
	 * @return Number of channels in every seizure
	 */
	public int getNumSzChannelsToAnalyze();
	
	/**
	 * @return A list containing all seizures in the dataset
	 */
	public List<Seizure> getSeizures();
	
	/**
	 * If LTSD matrix of this dataset is already cached, this method will simply return it.
	 * Otherwise, this method should calculate the LTSD matrix and store the matrix in the data base if there is any.
	 * @return LTSD matrix of this dataset
	 */
	public Matrix2D makeLTSDMatrix();
	
	/**
	 * If LTSD matrix of this dataset is already cached, this method will simply return it.
	 * Otherwise return null.
	 * @return LTSD matrix of this dataset
	 */
	public Matrix2D getLTSDMatrix();
	
	

}
