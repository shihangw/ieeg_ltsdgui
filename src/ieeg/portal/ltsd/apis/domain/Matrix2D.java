package ieeg.portal.ltsd.apis.domain;

import java.util.Arrays;

public class Matrix2D {
	
	public double[][] data;
	private int nRows, nColumns;
	
	public Matrix2D(int nRows, int nColumns) {
		data = new double[nRows][nColumns];
		this.nRows = nRows;
		this.nColumns = nColumns;
	}	
	
	public int getnRows() {
		return this.nRows;
	}

	public int getnColumns() {
		return this.nColumns;
	}

	public void printMatrix() {
		if (this.data.length == 0) {
			System.out.println("[]");
			return;
		}
		for (double[] row : data) {
			System.out.println(Arrays.toString(row));
		}
	}
	
	@Override
	public String toString() {
		if (this.data.length == 0)
			return "[]";
		StringBuilder sb = new StringBuilder();
		for (double[] row : data) {
			sb.append(Arrays.toString(row));
			sb.append('\n');
		}
		if (sb.charAt(sb.length()-1) == '\n')
			sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}
}
