package ieeg.portal.ltsd.apis.domain;

import ieeg.portal.ltsd.apis.gapStatistic.LTSDCalc;

import java.util.ArrayList;
import java.util.List;


/**
 * @author thomaswatkins
 * 
 * This class can be used to create a collection of Seizure objects from the annotations in a 
 * study on the portal so that the gap statistic calculation can then be performed upon them. This 
 * creates a distance matrix which is a measure of the similarity of each seizure compared to all
 * other seizures included in the Dataset object.
 * 
 * Note, the gap statistic is only a valid comparison within a single patient, during a single
 * recording.
 * 
 *TODO:
 *Proper java behaviour when throwing run time exceptions?
 *static factory method constructor to get independent construction of a seizure object, wait until portal info?
 *static factory method constructor to get independent construction of a DataSet object
 *think about parallelizing sections, candidates: computing LTSDval, making vector, hammingwindow splitting
 *faster fft algorithm
 *change doubles to floats to save on space?
 *Transposing arrays is annoying, format coming in from portal?
 *Error checking brainstorm.
 *
 */
public class SimpleDataSet implements DataSet {
	//Globals
	//index included in case this dataset is part of a DataSetCollection object, currently not
	//implemented correctly
	//int index;
	String identifier;
	//String filePath;
	int numSzChannelsToAnalyze = Integer.MAX_VALUE;
	List<Seizure> seizures;
	//the double array in the arrayList below consists of double[channels][szData]
	double[][] currentSeizure; 
	ArrayList<double[]> szVectorCache;
	Matrix2D distanceMatrix;
	LTSDCalc ltsdCalc;

	/**
	 * Constructor
	 * @param index
	 * @param identifier
	 * @param filePath
	 * @param szfileNames
	 */
	public SimpleDataSet (int index, String identifier, String filePath, List<Seizure> seizures, int windowSize, int nOverlap) {
		//this.index = index;
		this.identifier = identifier;
		//this.filePath = filePath;
		this.seizures = seizures;	
		szVectorCache = new ArrayList<double[]>();
		ltsdCalc = new LTSDCalc(windowSize, nOverlap);
		//Here we need to add the calculation to work out number of channels?
	}
	
	/**
	 *split function 
	 * Once all data has been correctly initialized a call to this method will calculate the LTSD matrix from the loaded in data
	 * and print it out to the console.
	 */
	public void makeMatrixCache() {
		//TODO what if there is no data here!
		this.numSzChannelsToAnalyze = 16;
		for (Seizure seizure : this.seizures) {

			this.currentSeizure = seizure.data;
			//Here we have the issue that we may want the start and end indices to vary for each
			//seizure file. For now assume they will be the same and worked out as above
			this.addToSzVectorCache(this.currentSeizure);
		}
	}
	
	/**
	 * Creates the distance matrix of values comparing each of the DataSets seizures to all their others.
	 * Uses already seizure vectors already computed for each seizure and held in the DataSet's vector cache.
	 * If the cache is of size one the function will return without computing an LTSD matrix and print
	 * an error message.
	 */
	public Matrix2D makeLTSDMatrix () {
		System.out.println("Called: makeLTSDMatrix");
		
		//If cache is empty, make cache automatically
		if (this.szVectorCache.size() == 0) {
			makeMatrixCache();
		}
		/*if (this.szVectorCache.size() == 1) {
			System.out.println("Only one element of seizure data held in vector cache, cannot make meaningful LTSDmatrix");
			return null;
		}*/
		int numSzs = szVectorCache.size();		
		distanceMatrix =  new Matrix2D(numSzs,numSzs);
		double [] Sx, Sy;
		double matrixVal;
		for (int i = 0; i < numSzs; i++) {			
			Sx = szVectorCache.get(i);
			for (int j = 0; j < numSzs; j++) {
				Sy = szVectorCache.get(j);
				matrixVal = ltsdCalc.calculateLTSDMatrixVal(Sx, Sy);
				//Fill in bottom triangle
				distanceMatrix.data[i][j] = matrixVal;
				//Fill in top triangle
				distanceMatrix.data[j][i] = matrixVal;	
			}
		}
		
		return this.distanceMatrix;
	}
	
	/**
	 * Populates a vector containing the power spectral density data in arrays of doubles for each seizure recorded in a single
	 * a DataSet.
	 * @return vectorCache, double[][] of power spectral density data for each seizure from DataSet.
	 */
	public void addToSzVectorCache (double[][] szFileData) {
		System.out.println("Called: makeSzVectorCache");
		//TODO check sizes are all the same. maybe when we read all the files in....
		szVectorCache.add(ltsdCalc.getAllChannelsRawSzVector(szFileData));
	}
	
	/**
	 * Prints precalculated distance matrix for this DataSet's seizures. If matrix is not precalculated, make one automatically.
	 */
	public void printDistanceMatrix () {
		if (this.distanceMatrix == null) {
			//System.out.println("Haven't calculated distance matrix for DataSet " + identifier);
			makeLTSDMatrix();
		}
		
		System.out.println("Dataset " + this.identifier);
		this.distanceMatrix.printMatrix();
	}

	@Override
	public String getIdentifier() {
		return this.identifier;
	}

	@Override
	public int getNumSzChannelsToAnalyze() {
		return this.numSzChannelsToAnalyze;
	}

	@Override
	public List<Seizure> getSeizures() {
		return this.seizures;
	}

	@Override
	public Matrix2D getLTSDMatrix() {
		return this.distanceMatrix;
	}


}
