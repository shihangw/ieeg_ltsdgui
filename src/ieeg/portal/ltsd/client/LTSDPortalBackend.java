package ieeg.portal.ltsd.client;

import ieeg.portal.ltsd.shared.LTSDAnalyzedSelectionData;
import ieeg.portal.ltsd.shared.LTSDMetaData;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("backend")

public interface LTSDPortalBackend extends RemoteService {
  LTSDMetaData getStudyFromLogin(String username, String password, 
			String domainURL, String nameOfStudy) throws IllegalArgumentException;
  
  LTSDAnalyzedSelectionData getSelectionCriteria(boolean isTime, int from, int to) throws IllegalArgumentException;
}
