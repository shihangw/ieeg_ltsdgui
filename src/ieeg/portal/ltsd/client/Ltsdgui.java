package ieeg.portal.ltsd.client;


import ieeg.portal.ltsd.shared.LTSDAnalyzedSelectionData;
import ieeg.portal.ltsd.shared.LTSDMetaData;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Ltsdgui implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
//	private final GreetingServiceAsync greetingService = GWT
//			.create(GreetingService.class);

	private final LTSDPortalBackendAsync ltsdPortalBackend = GWT
			.create(LTSDPortalBackend.class);

	private final Label ltsdStatusLabel = new Label("Status:");

	private final Label experimentValue = new Label("");
	private final Label patientValue = new Label("");
	private final Label dateValue = new Label("");
	private final Label lengthValue = new Label("");

	private final Label annotationsValue = new Label("");
	private final Label seizuresValue = new Label("");
	private final Label channelsValue = new Label("");
	private final HTML matrixVal = new HTML("");
	private final SimplePanel dendroPanel = new SimplePanel();
	
	private Panel getLTSDTopPanel() {
		HorizontalPanel topPanel = new HorizontalPanel();
		topPanel.setSpacing(20);
		topPanel.add(getLTSDLoginPanel());
		topPanel.add(getLTSDAnalyzePanel());
		return topPanel;
	}
	
	private Panel getLTSDLoginPanel(){
		final VerticalPanel loginPanel = new VerticalPanel();
		
		final Label usernameLabel = new Label("Username:");
		final TextBox usernameField = new TextBox();
		final Label passwordLabel = new Label("Password:");
		final PasswordTextBox passwordField = new PasswordTextBox();
		final Label domainLabel = new Label("Domain URL:");
		final TextBox domainField = new TextBox();
		final Label studynameLabel = new Label("Study Name:");
		final TextBox studynameField = new TextBox();
		
		final Button loginButton = new Button("LOGIN");
		final Button fillButton = new Button("Auto-fill credentials");
		
	
		Widget[] widgets = {
			usernameLabel,
			usernameField,
			passwordLabel,
			passwordField,
			domainLabel,
			domainField,
			studynameLabel,
			studynameField,
			loginButton,
			fillButton
		};
		
		for (int i = 0; i < widgets.length; i++)
			loginPanel.add(widgets[i]);
		
		
		class LoginHandler implements ClickHandler, KeyUpHandler {
			/**
			 * Fired when the user clicks on the sendButton.
			 */
			public void onClick(ClickEvent event) {
				sendLoginDetailsToServer();
			}

			/**
			 * Fired when the user types in the nameField.
			 */
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					sendLoginDetailsToServer();
				}
			}	
			

			/**
			 * Send the name from the nameField to the server and wait for a response.
			 * 
			 * Does this by firing an event on the event bus, and passing along a
			 * handler for the response.
			 */
			private void sendLoginDetailsToServer() {
				
				// First, we validate the input.
				//TODO display lack of error on the bottom
				//errorLabel.setText("");
				final String userName = usernameField.getText();
				final String password = passwordField.getText();
				final String domain = domainField.getText();
				final String studyName = studynameField.getText();

				// Then, we send the input to the server.
				loginButton.setEnabled(false);

				ltsdPortalBackend.getStudyFromLogin(userName, password, domain, studyName, new AsyncCallback<LTSDMetaData>() {
						@Override
						public void onFailure(Throwable caught){
							System.out.println("Failure");
							ltsdStatusLabel.setText("Status: failed to login as '" + userName + "'");
						}
						@Override
						public void onSuccess(LTSDMetaData result){
							System.out.println("Success");
							System.out.println(userName);
							ltsdStatusLabel.setText("Status: successful login as '" + userName + "'");
							seizuresValue.setText("" + result.getNumSeizures());
							channelsValue.setText("" + result.getNumChannels());
						}
				});
			}
		}

		class FillHandler implements ClickHandler {
			/**
			 * Fired when the user clicks on the sendButton.
			 */
			public void onClick(ClickEvent event) {
				usernameField.setText("watkinst");
				domainField.setText("https://view-qa.elasticbeanstalk.com/services");
				studynameField.setText("Study 017");
			}
		}
		
		loginButton.addClickHandler(new LoginHandler());
		fillButton.addClickHandler(new FillHandler());
		return loginPanel;
	}
	
	private Panel getLTSDAnalyzePanel() {
		HorizontalPanel dividerPanel = new HorizontalPanel();
		dividerPanel.setSpacing(30);
		dividerPanel.add(getLTSDInfoPanel());
		dividerPanel.add(getLTSDSelectionPanel());
		return dividerPanel;
	}
	
	private Panel getLTSDInfoPanel() {
		VerticalPanel infoPanel = new VerticalPanel();
		
		Label experimentLabel = new Label("Experiment:");
		Label patientLabel = new Label("Patient:");
		Label dateLabel = new Label("Date:");
		Label lengthLabel = new Label("Data length (hours):");
		
		Widget[] widgets = {
			experimentLabel,
			experimentValue,
			patientLabel,
			patientValue,
			dateLabel,
			dateValue,
			lengthLabel,
			lengthValue
		};
		
		for (int i = 0; i < widgets.length; i++)
			infoPanel.add(widgets[i]);
		
		return infoPanel;
	}
	
	private Panel getLTSDSelectionPanel() {
		VerticalPanel selectionPanel = new VerticalPanel();
		
		Label annotationsLabel = new Label("Number of annotations:");
		Label seizuresLabel = new Label("Number of seizure annotations:");
		Label channelsLabel = new Label("Number of channels:");
		
		FlowPanel selectionWrapper = new FlowPanel();
		final RadioButton timeButton = new RadioButton("selection", "Time");
		final RadioButton indicesButton = new RadioButton("selection", "Index");
		indicesButton.setValue(true);
		selectionWrapper.add(timeButton);
		selectionWrapper.add(indicesButton);
		
		final TextBox fromBox = new TextBox();
		final TextBox toBox = new TextBox();
		fromBox.setWidth("15px");
		toBox.setWidth("15px");
		
		selectionWrapper.add(fromBox);
		selectionWrapper.add(new Label(" to "));
		selectionWrapper.add(toBox);
		
		final Button analyzeButton = new Button("ANALYZE");
		
		Widget[] widgets = {
			annotationsLabel,
			annotationsValue,
			seizuresLabel,
			seizuresValue,
			channelsLabel,
			channelsValue,
			selectionWrapper,
			analyzeButton
		};
		
		for (int i = 0; i < widgets.length; i++)
			selectionPanel.add(widgets[i]);
		
		
		class SelectionHandler implements ClickHandler, KeyUpHandler {
			/**
			 * Fired when the user clicks on the sendButton.
			 */
			public void onClick(ClickEvent event) {
				sendSelectionDetailsToServer();
			}

			/**
			 * Fired when the user types in the nameField.
			 */
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					sendSelectionDetailsToServer();
				}
			}	
			

			/**
			 * Send the name from the nameField to the server and wait for a response.
			 * 
			 * Does this by firing an event on the event bus, and passing along a
			 * handler for the response.
			 */
			private void sendSelectionDetailsToServer() {
				
				// First, we validate the input.
				// TODO display lack of error on the bottom
				//errorLabel.setText("");
				// TODO checking for integers
				final boolean isTime = timeButton.getValue();
				final int fromIndex = Integer.parseInt(fromBox.getText());
				final int toIndex = Integer.parseInt(toBox.getText());

				// Then, we send the input to the server.
				analyzeButton.setEnabled(false);

				//TODO do we need to pass back the snapID?
				ltsdPortalBackend.getSelectionCriteria(isTime, fromIndex, toIndex, new AsyncCallback<LTSDAnalyzedSelectionData>() {
						@Override
						public void onFailure(Throwable caught){
							System.out.println("Failure");
						}
						@Override
						public void onSuccess(LTSDAnalyzedSelectionData result){
							System.out.println("Success");
							System.out.println(fromIndex);
							ltsdStatusLabel.setText("Status: successfully sent selection info to server '" + fromIndex + " " + toIndex + "'");
							matrixToHTML(result.getResultMatrix());
							updateLTSDDendroPanel(result.getImage());
						}
				});
			}
		}
		
		
		analyzeButton.addClickHandler(new SelectionHandler());

		return selectionPanel;
	}
	
	
	private Panel getLTSDMatrixPanel() {
		SimplePanel matrixPanel = new SimplePanel();
		matrixPanel.add(matrixVal);
		return matrixPanel;
	}

	
	private void matrixToHTML(double[][] simMatrix) {
		// Should make a request to the server back-end for matrix data later
		StringBuffer htmlTable = new StringBuffer();
		htmlTable.append("<table align=\"center\">");
		NumberFormat f = NumberFormat.getDecimalFormat();
		for (int r = 0; r < simMatrix.length; r++) {
			htmlTable.append("<tr>");
			for (int c = 0; c < simMatrix.length; c++) {
				htmlTable.append("<td>" + f.format(simMatrix[r][c]) + "</td>");
			}
			htmlTable.append("</tr>");
		}
		htmlTable.append("</table>");
		matrixVal.setHTML(htmlTable.toString());
	}
	
	private Panel getLTSDDendroPanel() {
		String url = "http://www.cs.nyu.edu/courses/summer08/"
				+ "G22.3033-002/fig_dendrogram.jpg";
		dendroPanel.add(new HTML("<img src=\"" + url + "\" width=\"400\""
				+ "height=\"400\"/>"));
		return dendroPanel;
	}
	
	private void updateLTSDDendroPanel(String base64Image) {
		dendroPanel.clear();
		dendroPanel.add(new HTML("<img src=\"" + base64Image + "\" width=\"400\""
				+ "height=\"400\"/>"));
		
	}
	
	private Panel getLTSDStatusPanel() {
		SimplePanel statusPanel = new SimplePanel(); 
		statusPanel.add(ltsdStatusLabel);
		return statusPanel;
	}

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		final DockPanel dockPanel = new DockPanel();
		dockPanel.add(getLTSDTopPanel(), DockPanel.NORTH);
		dockPanel.add(getLTSDStatusPanel(), DockPanel.SOUTH);
		dockPanel.add(getLTSDMatrixPanel(), DockPanel.WEST);
		dockPanel.add(getLTSDDendroPanel(), DockPanel.EAST);
		RootPanel.get("mainWindowContainer").add(dockPanel);
	}
}
