package ieeg.portal.ltsd.client;

import ieeg.portal.ltsd.shared.LTSDAnalyzedSelectionData;
import ieeg.portal.ltsd.shared.LTSDMetaData;

import com.google.gwt.user.client.rpc.AsyncCallback;


public interface LTSDPortalBackendAsync {
	
	void getStudyFromLogin(String un, String pw, String dom, String study, 
			AsyncCallback<LTSDMetaData> callback) throws IllegalArgumentException;
	
	void getSelectionCriteria(boolean isTime, int from, int to,
			AsyncCallback<LTSDAnalyzedSelectionData> callback);

}
